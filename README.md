# corail1.gitlab.io

Corail version 1.0, by Benoit Varillon and David Doose and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France

## Dependencies

    apt install doxygen
    apt install python3-pip
    apt install python3-sphinx
    pip3 install sphinx-book-theme
    pip3 install breathe
    pip3 install exhale
    pip3 install myst-parser

## To build localy

    mkdir -p corail/include
    cd corail/
    git clone https://gitlab.com/corail1/corail_core
    git clone https://gitlab.com/corail1/corail_examples
    cp -r corail_core/include/* include/
    cd ..
    mkdir build
    doxygen
    make html
