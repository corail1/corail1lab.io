.. 
	Corail version 1.0, by Benoit Varillon and David Doose
	and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
	Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France
..
	This file is part of the Corail project.
..
	Corail is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
..
	Corail is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.
..
	You should have received a copy of the GNU Lesser General Public License
	along with Corail.  If not, see <https://www.gnu.org/licenses/>.
	
Concepts
========
Corail aim to allow real-time programming using ROS2. Therefore this is needed to get control over priority and affinity of each tasks and to make them preemptible. To achieve this goals Corail use POSIX (pthread) and its real-time capabilities. Corail implementation also provides synchronization between tasks and a good management of time that prevent period drift.

Class Diagram
-------------
Corail was made following the architecture of the ROS2 rclcpp API, it uses the executor concept and implements a new one. To allow a complete control over the execution, new real-time node, timer, subscription and service implementations were also needed. The following diagram show all the classes contain in Corail.

.. image:: _static/img/Corail_class.png
   :width: 800
   :alt: Corail class diagram

Real-Time Executor
------------------
For now Corail provides only one execution policy which is to execute each task(timer/subscription/service) in its own thread.
Next releases will contains new policies using processes (one process per task, make groups of tasks).

Tasks Implementations
---------------------

Behaviours of every tasks (timer/subscription/service) are implemented using a states diagrams. This allows the control of synchronization and dynamic reconfiguration. Diagrams can be divided in two parts, the configuration part (in black) allow to control the creation and configuration of the thread and is the same for all types of tasks. The running part (in color) control the execution and is specific for each type of tasks.

Periodic Task
*************

The execution part of periodic tasks is quite simple and is made of two state, one which run the callback and one which wait for the next period.

.. image:: _static/img/PeriodicTask.png
   :width: 800
   :alt: PeriodicTask state diagram

Reactive Task
*************

For the reactive tasks (subscriptions and services) the execution part is a bit more complex than for periodic tasks. It is made of a polling state that check the arrival of new messages, then if no new message is received the next step is a waiting state that wait for the jitter period. If a message is received first an execution sate execute the callback and then a waiting state wait for the callback period before returning to polling state.

.. image:: _static/img/SubscriberTask.png
   :width: 800
   :alt: SubscriptionTask state diagram

This new implementation of subscriptions and services allow complete control of the execution and prevent cpu overload caused by a topic overflow. Furthermore switch from a reactive execution model to this new polling model makes the execution deterministic and allow to perform temporal analysis.

----
