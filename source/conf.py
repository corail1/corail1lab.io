#	Corail version 1.0, by Benoit Varillon and David Doose and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
#	Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France
#
#	This file is part of the Corail project.
#
#	Corail is free software: you can redistribute it and/or modify
#	it under the terms of the GNU Lesser General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#
#	Corail is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU Lesser General Public License for more details.
#
#	You should have received a copy of the GNU Lesser General Public License
#	along with Corail.  If not, see <https://www.gnu.org/licenses/>.
#----------------------------------------------------------------------------

# -- Project information -----------------------------------------------------

project = 'Corail'
copyright = '2020-2022, ONERA/ISAE-Supaero, Université de Toulouse, France'
author = 'Benoit Varillon David Doose Jean-Baptiste Chaudron Charles Lesir'
rst_epilog = 'Corail is free software: you can redistribute it and/or modify it under the terms of the :doc:`/COPYING.LESSER` as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.'
master_doc = 'index'

# The full version, including alpha/beta/rc tags
release = 'v1.1.1'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'breathe',
    'exhale',
    'myst_parser',
    'sphinx.ext.todo'
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_book_theme'
pygments_style = 'sphinx'

highlight_language = 'cpp'

source_suffix = {
    '.rst': 'restructuredtext',
    '.txt': 'markdown',
    '.md': 'markdown',
}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
#html_static_path = ['_static']

breathe_projects = {
    "Corail": "../build/doxygen/xml/"
}
breathe_default_project = "Corail"
beathe_default_members = ('members', 'undoc-members')

exhale_args = {
    # These arguments are required
    "containmentFolder":     "./api",
    "rootFileName":          "library_root.rst",
    "rootFileTitle":         "Library API",
    "doxygenStripFromPath":  "..",
    # Suggested optional arguments
    "createTreeView":        True,
    # TIP: if using the sphinx-bootstrap-theme, you need
    "treeViewIsBootstrap": True,
}

todo_include_todos = True
todo_emit_warnings = True
