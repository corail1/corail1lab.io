.. 
	Corail version 1.0, by Benoit Varillon and David Doose
	and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
	Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France
..
	This file is part of the Corail project.
..
	Corail is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
..
	Corail is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.
..
	You should have received a copy of the GNU Lesser General Public License
	along with Corail.  If not, see <https://www.gnu.org/licenses/>.
	
Examples
========

Publish-Subscrib :
-------------------
This example show the implementation of the publish subscribe exmample (https://docs.ros.org/en/foxy/Tutorials/Writing-A-Simple-Cpp-Publisher-And-Subscriber.html) using corail API. As you can see on the highlighted lines only some little class names and params changes are needed to switch from rclcpp to corail.

.. literalinclude:: ../corail/corail_examples/src/pub_sub.cpp
    :language: CPP
    :emphasize-lines: 33,36,39,49,55,58,60,67,75-77
    :linenos:

Rclcpp and Corail :
-------------------
This example show that corail realtime nodes and classic rclcpp nodes can co-exist in the same corail RealTimeExecutor.

.. literalinclude:: ../corail/corail_examples/src/pub_sub_ros.cpp
    :language: CPP
    :emphasize-lines: 33,55,78-79
    :linenos:

For more examples see the `corail_examples <https://gitlab.com/corail1/corail_examples>`_ package.

----
