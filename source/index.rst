.. 
	Corail version 1.0, by Benoit Varillon and David Doose
    and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
	Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France
..
	This file is part of the Corail project.
..
	Corail is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
..
	Corail is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.
..
	You should have received a copy of the GNU Lesser General Public License
	along with Corail.  If not, see <https://www.gnu.org/licenses/>.
   
.. Corail documentation master file, created by
   sphinx-quickstart on Fri May  7 11:05:37 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Corail, real-time with ROS2
==========================

Corail is a ROS2 library that was created to allow creation of real-time programs with ROS2. The api is very similar to the rclcpp api thus ROS2 users will not be confused when using the Corail api.


.. toctree::
   :titlesonly:
   :maxdepth: 1

   installation
   concepts
   examples
   api/library_root

----