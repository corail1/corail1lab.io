.. 
	Corail version 1.0, by Benoit Varillon and David Doose
    and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
	Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France
..
	This file is part of the Corail project.
..
	Corail is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
..
	Corail is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.
..
	You should have received a copy of the GNU Lesser General Public License
	along with Corail.  If not, see <https://www.gnu.org/licenses/>.
	
Installation
============

Requirements
------------

ROS2
***********
(Corail was tested with Foxy and Galactic)

This installation guide explain installation on Ubuntu/Debian using .deb packages. See the `ROS2 documentation <https://docs.ros.org/en/foxy/Installation.html#installationguide>`_ for further informations.

Add ROS repositories:

.. code-block:: console

    sudo apt update && sudo apt install curl gnupg2 lsb-release
    sudo curl -sSL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key  -o /usr/share/keyrings/ros-archive-keyring.gpg
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/ros-archive-keyring.gpg] http://packages.ros.org/ros2/ubuntu $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/ros2.list > /dev/null
    sudo apt update

Install ROS2 Foxy (or Galactic):

.. code-block:: console

    sudo apt install ros-foxy-ros-base

For autocompletion when using command line install argcomplete:

.. code-block:: console

    apt install -y python3-argcomplete


LTTng (2.12)
************

This installation guide is for Ubuntu 20.04 for other versions/distributions, see the `LTTng documentation <https://lttng.org/docs/v2.12/#doc-ubuntu>`_ .

Add the LTTng repository:

.. code-block:: console

    apt-add-repository ppa:lttng/stable-2.12
    apt-get update

Install the LTTng packages:

.. code-block:: console

    apt-get install lttng-tools
    apt-get install lttng-modules-dkms
    apt-get install liblttng-ust-dev

Realtime Configuration
**********************

The instructions below aim at configuring your Linux system to have the necessary rights to launch your components using the real-time scheduler.

Create a realtime group:

.. code-block:: console

    sudo groupadd realtime

Add a 'user' to the realtime group:

.. code-block:: console

    sudo usermod -a -G realtime user

Give realtime authorizations to the realtime group by adding following lines in /etc/security/limits.conf:

.. code-block:: console

    @realtime       soft    cpu             unlimited
    @realtime       -       rtprio          100
    @realtime       -       memlock         unlimited

Installation
------------

Corail is made of several packages, corail_core implementes the min functionalites and depends on corail_tracing that use LTTng to provide tracing functionalities. Corail_examples is not needed to build corail_core but contains examples and can be a good place to start using corail.

Create a Corail workspace and download the corail packages:

.. code-block:: console

   mkdir corail_ws
   cd corail_ws
   git clone -b v1.1.2 https://gitlab.com/corail1/corail_core.git
   git clone -b v1.0 https://gitlab.com/corail1/corail_tracing.git
   git clone -b v1.1 https://gitlab.com/corail1/corail_examples.git

Source your ROS2 installation and build this workspace:

.. code-block:: console

   source /opt/ros/foxy/setup.bash
   colcon build

Source the new built workspace and test your installation:

.. code-block:: console

   source ./insall/setup.bash
   ros2 run corail_examples pub_sub

if you see something like this your installation is succesfull:

.. code-block:: console

    [INFO] [1624290258.998868676] [Publisher]: message sended
    [INFO] [1624290259.098668851] [Subscriber]: message received
    [INFO] [1624290259.998631609] [Publisher]: message sended
    [INFO] [1624290260.098588136] [Subscriber]: message received
    [INFO] [1624290260.998628057] [Publisher]: message sended
    [INFO] [1624290261.098594423] [Subscriber]: message received
    [INFO] [1624290261.998640839] [Publisher]: message sended
    [INFO] [1624290262.098583640] [Subscriber]: message received
    [INFO] [1624290262.998621424] [Publisher]: message sended
    [INFO] [1624290263.098584060] [Subscriber]: message received
    [INFO] [1624290263.998637151] [Publisher]: message sended
    [INFO] [1624290264.098586120] [Subscriber]: message received

----
